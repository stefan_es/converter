# converter

### Version 2.0.1

### Description
Collection of converter functions

### Local Setup
- To install the project's dependencies, run `npm run deps`

### Tools
Linters:
- To lint the project's source code manually, run `npm run lint`

Tests:
- To run all tests, run `npm run test`

Documentation:
- To generate API docs, run `npm run docs`

Clean-Up scripts:
- To remove generated files, run `npm run clean`
- To remove generated files and dependencies, run `npm run wipe`
- To remove generated files and reinstall dependencies, run `npm run clean-install`

### How To Use
Include the libraries' modules into a Javascript environment and call their methods.

### Library Architecture:
- Library modules are located in `./modules`
- Project-related data that isn't logic is located in `./storage`
- Project-related data that isn't logic and should not go into version control is located in `./storage/private`
- Mandatory project data is located in `./resources`
- The library's default configuration is located in `./defaults.config.json`
- Examples' source is located in `./examples` and available in `./docs/examples` after building
- API docs are available in `./docs/api` after building
- Files in the project's root are meta files

### Symbology
- Models are capitalized CamelCase
- Variables are camelCase
- Database entities are lowercase & underscore separated
- Database relation labels are camelCase
- AJAX request parameters are lowercase & dash separated

### Code Rules
- Async/Await must not be used
- Every function & important constant requires properly integrated JSDocs
- Routes require a Promise.resolve() wrapper
- Error messages include a required short error summary followed by colon, a required detailed error message followed by a dot and an optional dump of what the error causing value actually is. Names of variables must be surrounded by single quotes
- A file structure for a specific topic (e.g. routes) could be _flat_ like `./routes.js` or _deep_ like `./routes/users.js`, `./routes/views.js`
- A "bundler" `index.js` file can be used in _deep_ file structures to load all sibling files at once if appropriate
