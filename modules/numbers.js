'use strict';

/**
 * Numbers module
 * @module modules/numbers
 */

const helpers = require('core-helpers');
const defaults = require('../defaults.config.json');

/**
 * Converts a number from any base to decimal
 * @private
 * @param base {number} Base to convert from
 * @param options {object} Options
 * @param options.letters {string[]} Letters to choose from as value representations above 9 in bases above 10
 * @throws TypeError 'invalid argument'
 * @throws RangeError 'invalid enum'
 * @returns {function(string): number} Conversion function
 */
function convertBaseToDec(base, options) {
  return (value) => {
    if (!helpers.validator.isString(value)) {
      throw new TypeError(`invalid argument: 'value' must be a string. Is: ${helpers.utility.getDump(value)}`);
    }
    const convert = (_value, isFraction) => {
      if (_value === 0) {
        return _value;
      }
      return String(_value).replace(/^-/, '').split('').reduce((res, baseXValue, index, digits) => {
        let indexOfLetter = 0;
        if (base > 10 && !helpers.validator.isNumber(parseInt(baseXValue, 10))) {
          indexOfLetter = options.letters.slice(0, base - 10).indexOf(baseXValue.toLowerCase());
          if (indexOfLetter === -1) {
            throw new RangeError(`invalid enum: Unknown character '${helpers.utility.getDump(baseXValue)}'`);
          }
        }
        return res + (
          (base ** (isFraction ? (index + 1) * -1 : (digits.length - (1 + index)))) *
          (baseXValue < 10 ? baseXValue : 10 + indexOfLetter)
        );
      }, 0);
    };
    const serializedValues = String(value).split('.');
    return (convert(serializedValues[0]) + convert(serializedValues[1] || 0, true)) * (value[0] === '-' ? -1 : 1);
  };
}

/**
 * Converts a decimal number to any base
 * @private
 * @param base {number} Base to convert to
 * @param options {object} Options
 * @param options.letters {string[]} Letters to choose from as value representations above 9 in bases above 10
 * @param options.accuracy {number} Maximum accuracy of fraction conversions
 * @throws TypeError 'invalid argument'
 * @returns {function(number): string} Conversion function
 */
function convertDecToBase(base, options) {
  return (value) => {
    if (!helpers.validator.isNumber(value) && !helpers.validator.isString(value)) {
      throw new TypeError(`invalid argument: 'value' must be a number or string. Is: ${helpers.utility.getDump(value)}`); // eslint-disable-line max-len
    }
    const convertFraction = (_value, convertedValue = '') => {
      if (_value <= 0 || convertedValue.length >= options.accuracy) {
        return convertedValue;
      }
      const baseValue = _value * base;
      return convertFraction(
        Number('0.'.concat(String(baseValue).split('.')[1] || 0)),
        convertedValue + (baseValue < 10 ? Math.floor(baseValue) : options.letters[Math.floor(baseValue) - 10])
      );
    };
    const convert = (_value) => {
      const powersOfBase = [1];
      while (powersOfBase[powersOfBase.length - 1] * base <= _value) {
        powersOfBase.push(base ** powersOfBase.length);
      }
      powersOfBase.reverse();
      let remainder = _value;
      return powersOfBase.reduce((res, powerOfBase) => {
        const timesOfPower = Math.floor(remainder / powerOfBase);
        if (timesOfPower) {
          remainder %= powerOfBase;
        }
        return res + (timesOfPower < 10 ? timesOfPower : options.letters[timesOfPower - 10]);
      }, '');
    };
    const serializedValue = Number(value);
    if (serializedValue - Math.floor(serializedValue)) {
      return `${serializedValue < 0 ? '-' : ''}${convert(Math.floor(Math.abs(serializedValue)))}.${convertFraction(Number('0.'.concat(String(serializedValue).split('.')[1])))}`; // eslint-disable-line max-len
    }
    return `${serializedValue < 0 ? '-' : ''}${convert((Math.abs(serializedValue)))}`;
  };
}

module.exports = {
  /**
   * Converts a number from one base into another base
   * @param number {number|string} Number to convert
   * @param from {number} Base to convert from
   * @param to {number} Base to convert to
   * @param [options] {object} Options
   * @param [options.additionalLetters] {string[]} Letters to be added to the default [A-Z]. Required for bases > 36
   * @param options.accuracy=22 {number} Maximum accuracy of fraction conversions
   * @throws TypeError 'invalid argument'
   * @throws RangeError 'duplicate enum'
   * @throws RangeError 'range overflow'
   * @returns {string} Number in new base
   */
  base: (number, from, to, options = {}) => {
    if (!helpers.validator.isNumber(number) && !helpers.validator.isString(number)) {
      throw new TypeError(`invalid argument: 'number' must be a number or string. Is: ${helpers.utility.getDump(number)}`); // eslint-disable-line max-len
    }
    if (!helpers.validator.isNumber(from)) {
      throw new TypeError(`invalid argument: 'from' must be a number. Is: ${helpers.utility.getDump(from)}`);
    }
    if (!helpers.validator.isNumber(to)) {
      throw new TypeError(`invalid argument: 'to' must be a number. Is: ${helpers.utility.getDump(to)}`);
    }
    if (!helpers.validator.isRealObject(options)) {
      throw new TypeError(`invalid argument: 'options' must be an object. Is: ${helpers.utility.getDump(options)}`);
    }
    const runtimeOptions = helpers.object.merge(defaults, options);
    if (runtimeOptions.letters && !helpers.validator.isArray(runtimeOptions.letters, letter => helpers.validator.isStringShorterThan(letter, 2))) { // eslint-disable-line max-len
      throw new TypeError(`invalid argument: 'runtimeOptions.letters' must be an array of characters. Is: ${helpers.utility.getDump(runtimeOptions.letters)}`); // eslint-disable-line max-len
    }
    if (!helpers.validator.isNumber(runtimeOptions.accuracy)) {
      throw new TypeError(`invalid argument: 'runtimeOptions.accuracy' must be a number. Is: ${helpers.utility.getDump(runtimeOptions.accuracy)}`); // eslint-disable-line max-len
    }
    if (helpers.validator.isArray(options.additionalLetters)) {
      options.additionalLetters.forEach((letter) => {
        if (!helpers.validator.isStringShorterThan(letter, 2)) {
          throw new TypeError(`invalid argument: 'letter' must be a character. Is: ${helpers.utility.getDump(letter)}`);
        }
        if (runtimeOptions.letters.includes(letter)) {
          throw new RangeError(`duplicate enum: Character '${helpers.utility.getDump(letter)}' already exists`);
        }
        runtimeOptions.letters.push(letter);
      });
    }
    [from, to].forEach((base) => {
      if (base - 10 > runtimeOptions.letters.length) {
        throw new RangeError(`range overflow: base must be an integer equal or lower than ${runtimeOptions.letters.length + 10}. Is: ${helpers.utility.getDump(base)}`); // eslint-disable-line max-len
      }
    });
    const decimal = from === 10 ? number : convertBaseToDec(from, runtimeOptions)(number.toString());
    if (to === 10) {
      return decimal.toString();
    }
    return convertDecToBase(to, runtimeOptions)(decimal);
  },
};
