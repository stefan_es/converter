'use strict';

const numberConverters = require('../modules/numbers');
const defaults = require('../defaults.config.json');

function removeTrailingZerosFromFraction(number) {
  const numberParts = number.toString().split('.');
  if (numberParts[1]) {
    for (let i = numberParts[1].length - 1; i >= 0; i -= 1) {
      if (numberParts[1][i] !== '0') {
        return numberParts[0].concat('.', numberParts[1].slice(0, i + 1));
      }
    }
  }
  return number;
}

module.exports = {
  onBefore: faker => {
    const base1 = faker.datatype.number({ min: 2, max: 36 });
    let base2 = base1;
    while (base2 === base1) {
      base2 = faker.datatype.number({ min: 2, max: 36 });
    }
    return [
      faker.datatype.number(1000000),
      base1,
      base2,
      faker.datatype.number({
        min: 0,
        max: 1000000,
        precision: 0.1 / (10 ** faker.datatype.number({ min: 2, max: defaults.accuracy })),
      }),
    ];
  },
  tests: {
    /**
     * Convert full decimal to base 2
     */
    decFullNumberToBase2: () => {
      return numberConverters.base(512, 10, 2) === (512).toString(2);
    },
    /**
     * Convert full base 2 to decimal
     */
    base2FullNumberToDec: () => {
      return numberConverters.base(111, 2, 10) === '7';
    },
    /**
     * Convert decimal to base 2
     */
    decToBase2: (value) => {
      return numberConverters.base(value, 10, 2) === (value).toString(2);
    },
    /**
     * Convert decimal to base 10 (no change)
     */
    decToBase10: (value) => {
      return numberConverters.base(value, 10, 10) === value.toString();
    },
    /**
     * Convert decimal to random base
     */
    decToRandomBase: (value, base) => {
      return parseInt(numberConverters.base(value, 10, base), base) === value;
    },
    /**
     * Convert zero decimal to random base
     */
    zeroDecToRandomBase: (value, base) => {
      return numberConverters.base(0.00, 10, base) === '0';
    },
    /**
     * Convert negative decimal to base 8
     */
    negativeDecToBase8: (value) => {
      return numberConverters.base(value * -1, 10, 8).toString() === (value * -1).toString(8);
    },
    /**
     * Convert base 2 to decimal
     */
    base2ToDec: (value) => {
      return numberConverters.base((value).toString(2), 2, 10) === value.toString();
    },
    /**
     * Convert random base to decimal
     */
    randomBaseToDec: (value, base) => {
      return numberConverters.base((value).toString(base), base, 10) === value.toString();
    },
    /**
     * Convert random base to random base
     */
    randomBaseToRandomBase: (value, base1, base2) => {
      return numberConverters.base((value).toString(base1), base1, base2) === (value).toString(base2);
    },
    /**
     * Convert zero random base to decimal
     */
    zeroRandomBaseToDec: (value, base) => {
      return numberConverters.base(0, base, 10) === '0';
    },
    /**
     * Convert negative base 8 to decimal
     */
    negativeBase8ToDec: (value) => {
      return numberConverters.base((value * -1).toString(8), 8, 10) === (value * -1).toString();
    },
    /**
     * Convert float decimal to base 2
     */
    floatDecToBase2: (value, base1, base2, float) => {
      const manualBinary = (float).toString(2);
      const indexOfPoint = manualBinary.indexOf('.');
      return numberConverters.base(float, 10, 2)
        .includes(removeTrailingZerosFromFraction(
          (indexOfPoint === -1 ?
            manualBinary :
            manualBinary.slice(0, indexOfPoint + 1 + defaults.accuracy)).slice(0, -1)
        ));
    },
    /**
     * Convert float decimal to periodic base 2
     */
    floatDecToBase2Periodic: () => {
      return numberConverters.base(0.819183, 10, 2, { accuracy: 52 }).includes((0.819183).toString(2).slice(0, -1));
    },
    /**
     * Convert float decimal to periodic base 5 with high accuracy
     */
    floatDecToBase5PeriodicHighAccuracy: () => {
      const accuracy = 500;
      const convertedValue = numberConverters.base(718.375, 10, 5, { accuracy });
      return convertedValue.length === convertedValue.split('.')[0].length + 1 + accuracy &&
        convertedValue.includes((718.375).toString(5).slice(0, -1));
    },
    /**
     * Convert negative float decimal to periodic base 21
     */
    negativeFloatDecToBase21Periodic: () => {
      return numberConverters.base(-9001.21, 10, 21).includes((-9001.21).toString(21).slice(0, -1));
    },
    /**
     * Convert float base 2 to decimal
     */
    floatBase2ToDec: (value, base1, base2, float) => {
      return numberConverters.base((float).toString(2), 2, 10) === float.toString();
    },
    /**
     * Convert float random base to random base
     */
    floatRandomBaseToRandomBase: (value, base1, base2, float) => {
      const manualBinary = (float).toString(base2);
      const indexOfPoint = manualBinary.indexOf('.');
      return numberConverters.base((float).toString(base1), base1, base2)
        .includes(removeTrailingZerosFromFraction(
          (indexOfPoint === -1 ?
            manualBinary :
            manualBinary.slice(0, indexOfPoint + 1 + defaults.accuracy)).slice(0, -1)
        ));
    },
    /**
     * Convert zero float random base to decimal
     */
    zeroFloatRandomBaseToDec: (value, base) => {
      return ['0', '0.00'].includes(numberConverters.base('0.00', base, 10));
    },
    /**
     * Convert negative float random base to decimal
     */
    negativeFloatRandomBaseToDec: (value, base1, base2, float) => {
      return numberConverters.base((float * -1).toString(base1), base1, 10)
        .includes((float * -1).toString().slice(0, -1));
    },
    /**
     * Convert float base 36 with leading zeros in fractional part to decimal
     */
    leadingZeroFloatBase36ToDec: () => {
      return numberConverters.base((607189.0026629).toString(36), 36, 10) === '607189.0026629';
    },
    /**
     * Convert negative random base to random base
     */
    negativeRandomBaseToRandomBase: (value, base1, base2) => {
      return numberConverters.base((value * -1).toString(base1), base1, base2)
        .includes(removeTrailingZerosFromFraction((value * -1).toString(base2)));
    },
    /**
     * Convert base 36 to base 50. Must pass due to extended letters
     */
    base36ToBase50: (value) => {
      // todo find way to verify
      return numberConverters.base(value, 36, 50, {
        additionalLetters: ['ä', 'ö', 'ü', '@', 'ł', '€', '¶', 'ŧ', '←', '→', 'þ', 'ſ', 'ð', 'đ'],
      });
    },
    /**
     * Convert a base to the same base
     */
    baseToSameBaseIsNoOp: (value, base1) => {
      return numberConverters.base(value.toString(base1), base1, base1) === value.toString(base1);
    },
    /**
     * Convert base 10 to base 37. Must throw RangeError
     */
    base10ToBase37Throw: () => {
      try {
        numberConverters.base(10000, 10, 37);
        return false;
      } catch (error) {
        return error.message.match(/^range overflow/i);
      }
    },
    /**
     * Convert non-existing letter to base 16. Must throw RangeError
     */
    invalidBase37ToBase16Throw: () => {
      try {
        numberConverters.base('¶', 30, 16, { additionalLetters: [] });
        return false;
      } catch (error) {
        return error.message.match(/^invalid enum/i);
      }
    },
    /**
     * Add duplicate letter for base 37 & 38. Must throw RangeError
     */
    addDuplicateLetterThrow: () => {
      try {
        numberConverters.base('ä', 38, 39, { additionalLetters: ['ä', 'ä', 'ö'] });
        return false;
      } catch (error) {
        return error.message.match(/^duplicate enum/i);
      }
    },
    /**
     * Add number as letter for base 38. Must throw TypeError
     */
    addNumberAsLetterThrow: () => {
      try {
        numberConverters.base('1', 38, 5, { additionalLetters: [5] });
        return false;
      } catch (error) {
        return error.message.match(/^invalid argument/i);
      }
    },
  },
  loops: 100000,
  concurrency: 1,
  loopConcurrency: 1,
};
