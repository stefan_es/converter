'use strict';

const pjson = require('./package.json');

module.exports = {
  opts: {
    encoding: 'utf8',
    destination: './docs/api',
    recurse: true,
    readme: 'README.md',
  },
  plugins: ['plugins/markdown'],
  source: {
    include: '.',
    includePattern: '.+\\.js(doc)?$',
    excludePattern: '((^|\\/|\\\\|)_|.+node_modules|.+resources|.+storage|.+docs|.+examples)',
  },
  tags: {
    allowUnknownTags: true,
  },
  templates: {
    default: {
      outputSourceFiles: !pjson.private,
      outputSourcePath: !pjson.private,
    },
  },
};
